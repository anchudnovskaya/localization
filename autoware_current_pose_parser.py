import os
absolute_path = os.path.dirname(os.path.abspath(__file__))
fin = open(absolute_path + '/autoware_0016/poses_calib_reversed.txt', 'r')
fout = open(absolute_path + '/autoware_0016/poses_calib_reversed_parsed.txt', 'w')
while True:
    line = fin.readline()
    if not line:
        break
    if line.find('position:') != -1:
        x_str = fin.readline()
        x = x_str[7:]
        y_str = fin.readline()
        y = y_str[7:]
        z_str = fin.readline()
        z = z_str[7:]
        res = str(round(float(x),3)) + ' ' + str(round(float(y),3)) + ' ' + str(round(float(z),3))
        fout.write(res + '\n')
