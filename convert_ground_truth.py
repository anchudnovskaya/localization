import os
absolute_path = os.path.dirname(os.path.abspath(__file__))
fin = open(absolute_path + '/poses/04.txt', 'r')
fout = open(absolute_path + '/poses/04_converted.txt', 'w')
count = 0
res = ''
while True:
    line = fin.readline()
    if not line:
        break
    for i in line.split(' '):
        res = res + str(round(float(i),3)) + ' '
    fout.write(res+'\n')
    res = ''
