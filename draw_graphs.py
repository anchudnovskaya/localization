import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
absolute_path = os.path.dirname(os.path.abspath(__file__))
fin = open(absolute_path + '/poses/04_final.txt', 'r')
fin_2 = open(absolute_path + '/autoware_0016/poses_calib_reversed_parsed.txt', 'r')
x_coords = []
y_coords = []
z_coords = []
while True:
    line = fin.readline()
    if not line:
        break
    x, y, z = line.split(' ')
    x_coords.append(float(x))
    y_coords.append(float(y))
    z_coords.append(float(z))

x_coords_2 = []
y_coords_2 = []
z_coords_2 = []
while True:
    line = fin_2.readline()
    if not line:
        break
    x, y, z = line.split(' ')
    x_coords_2.append(float(x))
    y_coords_2.append(float(y))
    z_coords_2.append(float(z))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim(-10,400)
ax.set_ylim(-10,400)
ax.set_zlim(-10,400)
ax.plot(x_coords, y_coords, z_coords, label = 'ground truth')
ax.plot(x_coords_2, y_coords_2, z_coords_2, label = 'autoware')
plt.savefig('calib_ground+autoware_same_axes.png')
plt.show()
