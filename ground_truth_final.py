import os
absolute_path = os.path.dirname(os.path.abspath(__file__))
fin = open(absolute_path + '/poses/04_converted.txt', 'r')
fout = open(absolute_path + '/poses/04_final.txt', 'w')
res = ''
while True:
    line = fin.readline()
    if not line:
        break
    nums = []
    for i in line.split(' '):
        nums.append(i)
    res = nums[11]+ ' ' + nums[3] + ' ' + nums[7]
    fout.write(res+'\n')
    res = ''
